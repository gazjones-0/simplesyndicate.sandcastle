﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.IO;
using SimpleSyndicate.IO;

namespace SimpleSyndicate.Sandcastle.Content
{
    /// <summary>
    /// Helper methods for version history.
    /// </summary>
    public static class VersionHistoryHelpers
    {
        /// <summary>
        /// Returns whether a version history exists in the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">Path to version history file.</param>
        /// <returns><c>true</c> if the version history exists; <c>false</c> otherwise.</returns>
        public static bool VersionHistoryExists(string path)
        {
            var versionHistory = VersionHistoryFilePathInternal(path);
            if (versionHistory != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns the version history path for the version history found in the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">Path to version history file.</param>
        /// <returns>The full path of the version history file.</returns>
        /// <exception cref="FileNotFoundException">Thrown when no version history file can be found.</exception>
        public static string VersionHistoryFilePath(string path)
        {
            var versionHistory = VersionHistoryFilePathInternal(path);
            if (versionHistory != null)
            {
                return versionHistory;
            }

            throw new FileNotFoundException("Couldn't find version history file from " + path.ValueOrNullStringIfNull());
        }

        /// <summary>
        /// Returns the version history file for the version history found in the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">Path to version history file.</param>
        /// <returns>The full path of the found version history file, or <c>null</c> if it can't be found.</returns>
        private static string VersionHistoryFilePathInternal(string path)
        {
            var commonPrefix = DetermineCommonPrefix(path);
            var projectName = DetermineProjectName(path);
            var searchPath = DetermineSearchPath(path);
            var helpDirectoryName = commonPrefix + ".Help";
            var versionHistoryFileName = projectName + ".aml";

            // see if it's in a directory in the path
            var foundFile = Find(searchPath, helpDirectoryName, versionHistoryFileName);
            if (foundFile == null)
            {
                // see if it's in one from the parent
                foundFile = Find(new DirectoryInfo(searchPath).Parent.FullName, helpDirectoryName, versionHistoryFileName);
                if (foundFile == null)
                {
                    // see if it's in one from the parent's parent
                    foundFile = Find(new DirectoryInfo(searchPath).Parent.Parent.FullName, helpDirectoryName, versionHistoryFileName);
                }
            }

            // if we've not found it, just look for a standard version history file
            if (foundFile == null)
            {
                versionHistoryFileName = "VersionHistory.aml";
                foundFile = Find(searchPath, helpDirectoryName, versionHistoryFileName);
                if (foundFile == null)
                {
                    // see if it's in one from the parent
                    foundFile = Find(new DirectoryInfo(searchPath).Parent.FullName, helpDirectoryName, versionHistoryFileName);
                }
            }

            // ensure we've the full path
            if (foundFile != null)
            {
                foundFile = Path.GetFullPath(foundFile);
            }

            return foundFile;
        }

        /// <summary>
        /// Returns the common prefix in the specified path; this is based on the last directory in the path up to the
        /// first period. E.g. for "C:\Some\Path\Test.Project\File.cs" it would be "Test".
        /// </summary>
        /// <param name="path">Path to process.</param>
        /// <returns>The prefix.</returns>
        private static string DetermineCommonPrefix(string path)
        {
            var directory = new DirectoryInfo(path);
            var prefix = directory.Name;
            if (prefix.IndexOf('.') >= 0)
            {
                prefix = prefix.Substring(0, prefix.IndexOf('.'));
            }

            return prefix;
        }

        /// <summary>
        /// Returns the project name in the specified path; this is the last directory in the path.
        /// </summary>
        /// <param name="path">Path to process.</param>
        /// <returns>The project name.</returns>
        private static string DetermineProjectName(string path) => new DirectoryInfo(path).Name;

        /// <summary>
        /// Returns the search path from the specified path.
        /// </summary>
        /// <param name="path">Path to process.</param>
        /// <returns>Search path.</returns>
        private static string DetermineSearchPath(string path) => new DirectoryInfo(path).FullName;

        /// <summary>
        /// Finds the file with the specified <paramref name="fileName"/> in the specified <paramref name="directoryName"/> if it
        /// exists in the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">Path to search in.</param>
        /// <param name="directoryName">Directory to find.</param>
        /// <param name="fileName">File to find in directory.</param>
        /// <returns>Path to the file, or <c>null</c> if it can't be found.</returns>
        private static string Find(string path, string directoryName, string fileName)
        {
            if (DirectoryHelpers.ContainsDirectory(path, directoryName))
            {
                return DirectoryHelpers.PathIfContainsFile(Path.Combine(path, directoryName), fileName, true);
            }

            return null;
        }
    }
}
