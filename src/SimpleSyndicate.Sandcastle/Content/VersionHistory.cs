﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Globalization;
using System.Xml;

namespace SimpleSyndicate.Sandcastle.Content
{
    /// <summary>
    /// Provides easy access to the contents of a version history file.
    /// </summary>
    public class VersionHistory
    {
        /// <summary>
        /// The version history contents.
        /// </summary>
        private XmlDocument _versionHistoryXml;

        /// <summary>
        /// Initializes a new instance of the <see cref="VersionHistory"/> class using the file at the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">The version history file to use.</param>
        public VersionHistory(string path)
        {
            FileName = path;
            Path = System.IO.Path.GetDirectoryName(System.IO.Path.GetFullPath(FileName));
            FileName = System.IO.Path.GetFileName(FileName);
            _versionHistoryXml = new XmlDocument();
            _versionHistoryXml.Load(System.IO.Path.Combine(Path, FileName));
        }

        /// <summary>
        /// Gets the path to the version history file.
        /// </summary>
        /// <value>Path to the version history file.</value>
        public string Path { get; private set; }

        /// <summary>
        /// Gets the filename of the version history file.
        /// </summary>
        /// <value>Version history filename.</value>
        public string FileName { get; private set; }

        /// <summary>
        /// Returns the contents of the latest section as text.
        /// </summary>
        /// <returns>The contents of the latest section</returns>
        public string LatestSectionAsText()
        {
            var namespaceManager = new XmlNamespaceManager(_versionHistoryXml.NameTable);
            namespaceManager.AddNamespace("doc", "http://ddue.schemas.microsoft.com/authoring/2003/5");
            var section = _versionHistoryXml.SelectSingleNode("//topic//doc:developerConceptualDocument//doc:section", namespaceManager);
            var changes = section.SelectSingleNode("doc:content//doc:list", namespaceManager);
            var changeItems = changes.SelectNodes("doc:listItem", namespaceManager);
            var latestSection = string.Empty;
            for (var index = 0; index < changeItems.Count; index++)
            {
                latestSection = AddItemToText(latestSection, NodeAsText(changeItems[index]));
            }

            return latestSection;
        }

        /// <summary>
        /// Returns the contents of the specified <paramref name="node"/> as text.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>The contents of the specified <paramref name="node"/> as text.</returns>
        private static string NodeAsText(XmlNode node)
        {
            var changeItem = node.InnerXml;
            changeItem = changeItem.Replace("<codeEntityReference>T:", "<codeEntityReference>");
            changeItem = changeItem.Replace("<codeEntityReference>N:", "<codeEntityReference>");
            changeItem = changeItem.Replace("`1</codeEntityReference>", "</codeEntityReference>");
            changeItem = changeItem.Replace("`2</codeEntityReference>", "</codeEntityReference>");
            changeItem = changeItem.Replace("`3</codeEntityReference>", "</codeEntityReference>");
            changeItem = changeItem.Replace("`4</codeEntityReference>", "</codeEntityReference>");
            changeItem = changeItem.Replace("`5</codeEntityReference>", "</codeEntityReference>");
            changeItem = changeItem.Replace("`6</codeEntityReference>", "</codeEntityReference>");
            changeItem = changeItem.Replace("`7</codeEntityReference>", "</codeEntityReference>");
            changeItem = changeItem.Replace("`8</codeEntityReference>", "</codeEntityReference>");
            changeItem = changeItem.Replace("`9</codeEntityReference>", "</codeEntityReference>");
            changeItem = changeItem.Replace("`10</codeEntityReference>", "</codeEntityReference>");
            var newNode = new XmlDocument
            {
                InnerXml = changeItem
            };
            return newNode.InnerText.Trim();
        }

        /// <summary>
        /// Adds the specified <paramref name="item"/> to the specified <paramref name="text"/>.
        /// </summary>
        /// <param name="text">Current text.</param>
        /// <param name="item">Item to add.</param>
        /// <returns>The current text with the item added to it.</returns>
        private static string AddItemToText(string text, string item)
        {
            text = AddItemSeparatorToText(text);
            if (!string.IsNullOrEmpty(text))
            {
                return text + item.Substring(0, 1).ToLower(CultureInfo.CurrentCulture) + item.Substring(1);
            }

            return item;
        }

        /// <summary>
        /// Adds an item separator to the text.
        /// </summary>
        /// <param name="text">Current text.</param>
        /// <returns>The current text with an item separator added to it.</returns>
        private static string AddItemSeparatorToText(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                if (text.EndsWith(".", StringComparison.OrdinalIgnoreCase))
                {
                    return text.Substring(0, text.Length - 1) + "; ";
                }

                return text + "; ";
            }

            return text;
        }
    }
}
