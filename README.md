# README #

SimpleSyndicate.Sandcastle NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.Sandcastle

### What is this repository for? ###

* Functionality for working with Sandcastle Help File Builder.

### How do I get started? ###

* See the documentation at http://gazooka_g72.bitbucket.org/SimpleSyndicate

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.sandcastle/issues?status=new&status=open
